# Relveo

Relveo is a simple [Spring Boot](https://projects.spring.io/spring-boot/) MVC
with [Thymeleaf](https://www.thymeleaf.org) and [HTMX](https://htmx.org)

