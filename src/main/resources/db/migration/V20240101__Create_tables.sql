drop table if exists relveo.electricity_index_statements;
create table relveo.electricity_index_statements
(
    id             bigserial,
    statement_date date,
    day_index      numeric(19, 2),
    night_index    numeric(19, 2),
    user_id        text,
    primary key (id)
);
create index electricity_userid_idx on relveo.electricity_index_statements(user_id);

drop table if exists relveo.heating_index_statements;
create table relveo.heating_index_statements
(
    id             bigserial,
    statement_date date,
    height_of_tank numeric(19, 2),
    user_id        text,
    primary key (id)
);
create index heating_userid_idx on relveo.heating_index_statements(user_id);

drop table if exists relveo.water_index_statements;
create table relveo.water_index_statements
(
    id             bigserial,
    statement_date date,
    water_index    numeric(19, 2),
    user_id        text,
    primary key (id)
);
create index water_userid_idx on relveo.water_index_statements(user_id);

drop table if exists relveo.user_preferences;
create table relveo.user_preferences
(
    id               text,
    last_login       date,
    last_login_time  time without time zone,
    tank_type        text,
    width_of_tank    numeric(19, 2),
    length_of_tank   numeric(19, 2),
    diameter_of_tank numeric(19, 2),
    primary key (id)
);