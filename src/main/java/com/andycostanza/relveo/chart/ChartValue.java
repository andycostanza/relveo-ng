package com.andycostanza.relveo.chart;


import java.math.BigDecimal;


public record ChartValue(String name, BigDecimal value) {
}
