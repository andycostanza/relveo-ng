package com.andycostanza.relveo.chart;

import java.util.List;

public record ChartContainer(String name, List<ChartValue> series) {
}
