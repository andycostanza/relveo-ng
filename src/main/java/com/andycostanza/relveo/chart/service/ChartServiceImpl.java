package com.andycostanza.relveo.chart.service;

import com.andycostanza.relveo.chart.ChartContainer;
import com.andycostanza.relveo.chart.ChartValue;
import com.andycostanza.relveo.dashboard.DashboarChartType;
import com.andycostanza.relveo.dashboard.DashboarChartView;
import com.andycostanza.relveo.dashboard.DashboardFilter;
import com.andycostanza.relveo.electricity.ElectricityIndexStatement;
import com.andycostanza.relveo.heating.HeatingIndexStatement;
import com.andycostanza.relveo.userpreference.UserPreference;
import com.andycostanza.relveo.water.WaterIndexStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;

@Service

public class ChartServiceImpl implements ChartService {
    Logger log = LoggerFactory.getLogger(ChartServiceImpl.class);

    @Override
    public List<ChartContainer> electricityConsumptionCalculator(List<ElectricityIndexStatement> indexStatements) {
        List<ChartContainer> chartContainers = new ArrayList<>();
        List<ChartValue> dayConsumptionChartValue = new ArrayList<>();
        List<ChartValue> nightConsumptionChartValue = new ArrayList<>();
        List<ChartValue> totalConsumptionChartValue = new ArrayList<>();
        if (!CollectionUtils.isEmpty(indexStatements)) {
            Collections.sort(indexStatements);
            for (int i = 1; i < indexStatements.size(); i++) {
                ElectricityIndexStatement indexStatement = indexStatements.get(i);
                ElectricityIndexStatement previousIndexStatement = indexStatements.get(i - 1);
                if(indexStatement!=null && previousIndexStatement !=null) {
                    BigDecimal indexStatementDayIndex = indexStatement.getDayIndex() != null ? indexStatement.getDayIndex() : BigDecimal.ZERO;
                    BigDecimal previousIndexStatementDayIndex = previousIndexStatement.getDayIndex() !=null ? previousIndexStatement.getDayIndex() : BigDecimal.ZERO;
                    BigDecimal dayConsumption = indexStatementDayIndex
                            .subtract(previousIndexStatementDayIndex);
                    log.debug("Day Consumption of " + indexStatement.getStatementDate()
                            .toString() + " is " + dayConsumption);
                    dayConsumptionChartValue.add(new ChartValue(indexStatement.getStatementDate()
                            .toString(), dayConsumption));
                    BigDecimal indexStatementNightIndex = indexStatement.getNightIndex() != null ? indexStatement.getNightIndex() : BigDecimal.ZERO;
                    BigDecimal previousIndexStatementNightIndex = previousIndexStatement.getNightIndex() !=null ? previousIndexStatement.getNightIndex() : BigDecimal.ZERO;
                    BigDecimal nightConsumption = indexStatementNightIndex.subtract(previousIndexStatementNightIndex);
                    log.debug("Night Consumption of " + indexStatement.getStatementDate()
                            .toString() + " is " + nightConsumption);
                    nightConsumptionChartValue.add(new ChartValue(indexStatement.getStatementDate()
                            .toString(), nightConsumption));
                    totalConsumptionChartValue.add(new ChartValue(indexStatement.getStatementDate()
                            .toString(), dayConsumption.add(nightConsumption)));
                }
            }
            chartContainers.add(new ChartContainer("Day Consumption", dayConsumptionChartValue));
            chartContainers.add(new ChartContainer("Night Consumption", nightConsumptionChartValue));
            chartContainers.add(new ChartContainer("Total Consumption", totalConsumptionChartValue));
        }
        return chartContainers;
    }

    @Override
    public List<ChartContainer> heatingConsumptionCalculator(List<HeatingIndexStatement> indexStatements, UserPreference userPreference) throws IllegalArgumentException {
        List<ChartContainer> chartContainers = new ArrayList<>();
        List<ChartValue> consumptionChartValue = new ArrayList<>();
        if (!CollectionUtils.isEmpty(indexStatements)
                && userPreference != null
                && userPreference.getSurfaceOfTank().isPresent()) {
            //Surface cm² * 1cm = cm³ -> cm³/1.000.000 = m³ -> 1m³ = 1000l
            BigDecimal literByCentimerCoefficient = userPreference.getSurfaceOfTank()
                    .get()
                    .divide(BigDecimal.valueOf(1000D), 0, RoundingMode.HALF_UP)
                    .negate();
            Collections.sort(indexStatements);
            log.info("Coef {} for user {}", literByCentimerCoefficient, userPreference.getId());
            for (int i = 1; i < indexStatements.size(); i++) {
                HeatingIndexStatement indexStatement = indexStatements.get(i);
                HeatingIndexStatement previousIndexStatement = indexStatements.get(i - 1);

                BigDecimal consumption = (indexStatement.getHeightOfTank()
                        .subtract(previousIndexStatement.getHeightOfTank())).multiply(literByCentimerCoefficient);
                log.debug("Heating Consumption of " + indexStatement.getStatementDate()
                        .toString() + " is " + consumption);
                consumptionChartValue.add(new ChartValue(indexStatement.getStatementDate()
                        .toString(), consumption.compareTo(BigDecimal.ZERO) < 0 ? BigDecimal.ZERO : consumption));
            }
            chartContainers.add(new ChartContainer("Heating consumption", consumptionChartValue));
        }
        return chartContainers;
    }

    @Override
    public List<ChartContainer> waterConsumptionCalculator(List<WaterIndexStatement> indexStatements) {
        List<ChartContainer> chartContainers = new ArrayList<>();
        List<ChartValue> consumptionChartValue = new ArrayList<>();
        if (!CollectionUtils.isEmpty(indexStatements)) {
            Collections.sort(indexStatements);
            for (int i = 1; i < indexStatements.size(); i++) {
                WaterIndexStatement indexStatement = indexStatements.get(i);
                WaterIndexStatement previousIndexStatement = indexStatements.get(i - 1);
                BigDecimal consumption = indexStatement.getWaterIndex()
                        .subtract(previousIndexStatement.getWaterIndex());
                log.debug("Water Consumption of " + indexStatement.getStatementDate()
                        .toString() + " is " + consumption);
                consumptionChartValue.add(new ChartValue(indexStatement.getStatementDate()
                        .toString(), consumption.compareTo(BigDecimal.ZERO) < 0 ? BigDecimal.ZERO : consumption));
            }
            chartContainers.add(new ChartContainer("Water consumption", consumptionChartValue));
        }
        return chartContainers;
    }

    @Override
    public List<ChartContainer> dashboardConsumptionCalculator(DashboardFilter filter, List<ElectricityIndexStatement> electricityIndexStatements, List<HeatingIndexStatement> heatingIndexStatements, UserPreference userPreference, List<WaterIndexStatement> waterIndexStatements) {
        if (DashboarChartType.ANNUAL.equals(filter.type())) {
            List<ChartContainer> annualChartContainers = new ArrayList<>();
            if (DashboarChartView.GLOBAL.equals(filter.view()) || DashboarChartView.ELECTRICITY.equals(filter.view())) {
                List<ChartContainer> electricityConsumptionCalculator = this.electricityConsumptionCalculator(
                        electricityIndexStatements);

                BigDecimal annualElectricityConsumption = electricityConsumptionCalculator.stream()
                        .filter(container -> "Total Consumption".equals(container.name()))
                        .map(ChartContainer::series)
                        .flatMap(List::stream)
                        .map(ChartValue::value)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                annualChartContainers.add(new ChartContainer("Electricity",
                        List.of(new ChartValue(String.valueOf(filter.year()), annualElectricityConsumption))));
            }
            if (DashboarChartView.GLOBAL.equals(filter.view()) || DashboarChartView.HEATING.equals(filter.view())) {
                List<ChartContainer> heatingConsumptionCalculator = this.heatingConsumptionCalculator(heatingIndexStatements,
                        userPreference);
                BigDecimal annualHeatingConsumption = heatingConsumptionCalculator.stream()
                        .map(ChartContainer::series)
                        .flatMap(List::stream)
                        .map(ChartValue::value)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                annualChartContainers.add(new ChartContainer("Heating",
                        List.of(new ChartValue(String.valueOf(filter.year()), annualHeatingConsumption))));
            }
            if (DashboarChartView.GLOBAL.equals(filter.view()) || DashboarChartView.WATER.equals(filter.view())) {
                List<ChartContainer> waterConsumptionCalculator = this.waterConsumptionCalculator(waterIndexStatements);
                BigDecimal annualWaterConsumption = waterConsumptionCalculator.stream()
                        .map(ChartContainer::series)
                        .flatMap(List::stream)
                        .map(ChartValue::value)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                annualChartContainers.add(new ChartContainer("Water",
                        List.of(new ChartValue(String.valueOf(filter.year()), annualWaterConsumption))));
            }
            return annualChartContainers;

        } else if (DashboarChartType.MONTHLY.equals(filter.type())) {
            List<ChartContainer> monthlyChartContainers = new ArrayList<>();
            if (DashboarChartView.GLOBAL.equals(filter.view()) || DashboarChartView.ELECTRICITY.equals(filter.view())) {
                Map<Month, List<ElectricityIndexStatement>> electricityIndexStatementsByMonth = electricityIndexStatements.stream()
                        .collect(Collectors.groupingBy(statement -> statement.getStatementDate()
                                .getMonth()));
                Map<Month, List<ElectricityIndexStatement>> electricityIndexStatementsSortedByMonth = new TreeMap<>(
                        electricityIndexStatementsByMonth);
                List<ChartValue> monthlyElectricityChartValue = new ArrayList<>();
                electricityIndexStatementsSortedByMonth.forEach((k, v) -> {
                    List<ChartContainer> electricityConsumptionCalculator = this.electricityConsumptionCalculator(v);
                    BigDecimal monthlyElectricityConsumption = electricityConsumptionCalculator.stream()
                            .filter(container -> "Total Consumption".equals(container.name()))
                            .map(ChartContainer::series)
                            .flatMap(List::stream)
                            .map(ChartValue::value)
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    monthlyElectricityChartValue.add(new ChartValue(k.name(), monthlyElectricityConsumption));

                });
                ChartContainer electricityChartContainer = new ChartContainer("Electricity", monthlyElectricityChartValue);
                monthlyChartContainers.add(electricityChartContainer);
            }

            if (DashboarChartView.GLOBAL.equals(filter.view()) || DashboarChartView.HEATING.equals(filter.view())) {
                Map<Month, List<HeatingIndexStatement>> heatingIndexStatementsByMonth = heatingIndexStatements.stream()
                        .collect(Collectors.groupingBy(statement -> statement.getStatementDate()
                                .getMonth()));
                Map<Month, List<HeatingIndexStatement>> heatingIndexStatementsSortedByMonth = new TreeMap<>(
                        heatingIndexStatementsByMonth);
                List<ChartValue> monthlyHeatingChartValue = new ArrayList<>();
                heatingIndexStatementsSortedByMonth.forEach((k, v) -> {
                    List<ChartContainer> heatingConsumptionCalculator = this.heatingConsumptionCalculator(v,
                            userPreference);
                    BigDecimal monthlyHeatingConsumption = heatingConsumptionCalculator.stream()
                            .map(ChartContainer::series)
                            .flatMap(List::stream)
                            .map(ChartValue::value)
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    monthlyHeatingChartValue.add(new ChartValue(k.name(), monthlyHeatingConsumption));

                });
                ChartContainer heatingChartContainer = new ChartContainer("Heating", monthlyHeatingChartValue);
                monthlyChartContainers.add(heatingChartContainer);
            }
            if (DashboarChartView.GLOBAL.equals(filter.view()) || DashboarChartView.WATER.equals(filter.view())) {
                Map<Month, List<WaterIndexStatement>> waterIndexStatementsByMonth = waterIndexStatements.stream()
                        .collect(Collectors.groupingBy(statement -> statement.getStatementDate()
                                .getMonth()));
                Map<Month, List<WaterIndexStatement>> waterIndexStatementsSortedByMonth = new TreeMap<>(
                        waterIndexStatementsByMonth);
                List<ChartValue> monthlyWaterChartValue = new ArrayList<>();
                waterIndexStatementsSortedByMonth.forEach((k, v) -> {
                    List<ChartContainer> waterConsumptionCalculator = this.waterConsumptionCalculator(v);
                    BigDecimal monthlyWaterConsumption = waterConsumptionCalculator.stream()
                            .map(ChartContainer::series)
                            .flatMap(List::stream)
                            .map(ChartValue::value)
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    monthlyWaterChartValue.add(new ChartValue(k.name(), monthlyWaterConsumption));

                });
                ChartContainer waterChartContainer = new ChartContainer("Water", monthlyWaterChartValue);
                monthlyChartContainers.add(waterChartContainer);
            }
            return monthlyChartContainers;
        } else {
            return Collections.emptyList();
        }
    }


}
