package com.andycostanza.relveo.water;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by andy on 24/03/17.
 */
@Entity
@Table(name = "water_index_statements", schema = "relveo")
public class WaterIndexStatement implements Serializable, Comparable<WaterIndexStatement> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate statementDate;
    private BigDecimal waterIndex;
    private String userId;

    public WaterIndexStatement() {
    }

    public WaterIndexStatement(Long id, LocalDate statementDate, BigDecimal waterIndex, String userId) {
        this.id = id;
        this.statementDate = statementDate;
        this.waterIndex = waterIndex;
        this.userId = userId;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public int compareTo(WaterIndexStatement w) {
        return getStatementDate().compareTo(w.getStatementDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStatementDate() {
        return statementDate;
    }

    public void setStatementDate(LocalDate statementDate) {
        this.statementDate = statementDate;
    }

    public BigDecimal getWaterIndex() {
        return waterIndex;
    }

    public void setWaterIndex(BigDecimal waterIndex) {
        this.waterIndex = waterIndex;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WaterIndexStatement that)) return false;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getStatementDate() != null ? !getStatementDate().equals(that.getStatementDate()) : that.getStatementDate() != null)
            return false;
        if (getWaterIndex() != null ? !getWaterIndex().equals(that.getWaterIndex()) : that.getWaterIndex() != null)
            return false;
        return getUserId() != null ? getUserId().equals(that.getUserId()) : that.getUserId() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getStatementDate() != null ? getStatementDate().hashCode() : 0);
        result = 31 * result + (getWaterIndex() != null ? getWaterIndex().hashCode() : 0);
        result = 31 * result + (getUserId() != null ? getUserId().hashCode() : 0);
        return result;
    }

    public static class Builder {
        private Long id;
        private LocalDate statementDate;
        private BigDecimal waterIndex;
        private String userId;

        public Builder() {
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder statementDate(LocalDate statementDate) {
            this.statementDate = statementDate;
            return this;
        }

        public Builder waterIndex(BigDecimal waterIndex) {
            this.waterIndex = waterIndex;
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public WaterIndexStatement build() {
            return new WaterIndexStatement(id, statementDate, waterIndex, userId);
        }
    }
}
