package com.andycostanza.relveo.water;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.time.LocalDate;


public record WaterIndexStatementCsv(@JsonFormat(pattern = "yyyy-MM-dd") LocalDate statementDate,
                                     BigDecimal waterIndex) {
}
