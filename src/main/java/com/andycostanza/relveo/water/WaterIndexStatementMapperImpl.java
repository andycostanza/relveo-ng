package com.andycostanza.relveo.water;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WaterIndexStatementMapperImpl implements WaterIndexStatementMapper {
    private final WaterIndexStatementRepository repository;

    public WaterIndexStatementMapperImpl(WaterIndexStatementRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<WaterIndexStatement> toEntities(List<WaterIndexStatementCsv> csv, String userId) {
        return csv.stream()
                .map(waterIndexStatementCsv -> {
                    Optional<WaterIndexStatement> entity = repository.findByUserIdAndStatementDate(userId,
                            waterIndexStatementCsv.statementDate());
                    if (entity.isPresent()) {
                        entity.get()
                                .setWaterIndex(waterIndexStatementCsv.waterIndex());
                        return entity.get();
                    } else {
                        return WaterIndexStatement.builder()
                                .statementDate(waterIndexStatementCsv.statementDate())
                                .waterIndex(waterIndexStatementCsv.waterIndex())
                                .userId(userId)
                                .build();
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<WaterIndexStatementCsv> toCsv(List<WaterIndexStatement> entities) {
        return entities.stream()
                .map(waterIndexStatement -> new WaterIndexStatementCsv(waterIndexStatement.getStatementDate(),
                        waterIndexStatement.getWaterIndex()))
                .collect(Collectors.toList());
    }
}
