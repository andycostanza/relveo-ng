package com.andycostanza.relveo.water;

import com.andycostanza.relveo.chart.ChartContainer;
import com.andycostanza.relveo.chart.service.ChartService;
import com.andycostanza.relveo.utils.CsvFileMapper;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/water")
public class WaterIndexStatementController {
    private final WaterIndexStatementRepository repository;
    private final ChartService chartService;
    private final CsvFileMapper csvFileMapper;
    private final WaterIndexStatementMapper mapper;
    Logger log = LoggerFactory.getLogger(WaterIndexStatementController.class);

    public WaterIndexStatementController(WaterIndexStatementRepository repository, ChartService chartService, CsvFileMapper csvFileMapper, WaterIndexStatementMapper mapper) {
        this.repository = repository;
        this.chartService = chartService;
        this.csvFileMapper = csvFileMapper;
        this.mapper = mapper;
    }

    @ModelAttribute("requestURI")
    public String requestURI(final HttpServletRequest request) {
        return request.getRequestURI();
    }

    private static String getUserId(OidcUser principal) {
        return Base64.getEncoder()
                .encodeToString(principal.getSubject()
                        .getBytes(Charset.defaultCharset()));
    }

    @PostMapping(value = "/import")
    public String importMultipart(
            @AuthenticationPrincipal OidcUser principal, @RequestParam("file") MultipartFile file, Model model) {

        try {
            String userId = getUserId(principal);
            if (file.getSize() > 0) {
                List<WaterIndexStatementCsv> csvDatas = csvFileMapper.read(WaterIndexStatementCsv.class,
                        file.getInputStream());
                List<WaterIndexStatement> entities = mapper.toEntities(csvDatas, userId);
                repository.saveAll(entities);

                return "redirect:/water";
            } else {
                model.addAttribute("errorMessage", "The CSV file is empty");
                return "error";
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            model.addAttribute("errorMessage", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/export")
    public void exportCsv(@AuthenticationPrincipal OidcUser principal, HttpServletResponse response) {
        try {
            String userId = getUserId(principal);
            List<WaterIndexStatement> entities = repository.findByUserIdOrderByStatementDateDesc(userId);
            List<WaterIndexStatementCsv> csv = mapper.toCsv(entities);
            byte[] bytes = csvFileMapper.write(WaterIndexStatementCsv.class, csv);

            response.setContentType(MediaType.parseMediaType("text/csv")
                    .getType());

            response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=water-data_" + LocalDateTime.now()
                            .toEpochSecond(ZoneOffset.UTC) + ".csv");
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Expires", "0");
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(bytes);
            outputStream.close();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @GetMapping("/wipe")
    public String wipe(@AuthenticationPrincipal OidcUser principal) {
        String userId = getUserId(principal);
        repository.deleteAllInBatch(repository.findByUserIdOrderByStatementDateDesc(userId));
        return "redirect:/water";
    }

    @GetMapping
    public String water(Model model,
                        @AuthenticationPrincipal OidcUser principal,
                        @RequestParam(name = "page", required = false) Integer page,
                        @RequestParam(name = "size", required = false) Integer size) {
        refreshDataInModel(model, principal, page, size);
        return "water";
    }

    private void refreshDataInModel(Model model, OidcUser principal, Integer page, Integer size) {
        if (principal != null) {
            //model.addAttribute("profile", principal.getClaims());
            String userId = getUserId(principal);
            log.info("userId {}", userId);
            int currentPage = page != null ? page : 0;
            int currentSize = size != null ? size : 10;
            Page<WaterIndexStatement> datas = repository.findByUserId(userId,
                    PageRequest.of(currentPage, currentSize, Sort.by(Sort.Direction.DESC, "statementDate")));

            model.addAttribute("datas", datas);

            int totalPages = datas.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                        .boxed()
                        .collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
            model.addAttribute("statement",
                    WaterIndexStatement.builder()
                            .userId(userId)
                            .build());
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("currentSize", currentSize);
            List<ChartContainer> dataset = chartService.waterConsumptionCalculator(repository.findTop53ByUserIdOrderByStatementDateDesc(
                    userId));
            model.addAttribute("dataset", dataset);

        }
    }

    @GetMapping("/{id}")
    public String findOne(
            @PathVariable("id") Long id, Model model,
            @RequestParam(name = "page", required = false) Integer currentPage,
            @RequestParam(name = "size", required = false) Integer currentSize) {
        Optional<WaterIndexStatement> statement = repository.findById(id);
        if (statement.isPresent()) {
            model.addAttribute("statement", statement.get());
            model.addAttribute("currentPage", currentPage);
            model.addAttribute("currentSize", currentSize);
            return "fragments/water/update :: update";
        }
        return "fragments/error :: error";
    }

    @PostMapping
    public String upsert(
            @ModelAttribute WaterIndexStatement statement, Model model,
            @AuthenticationPrincipal OidcUser principal,
            @RequestParam(name = "page", required = false) Integer page,
            @RequestParam(name = "size", required = false) Integer size) {
        repository.save(statement);
        refreshDataInModel(model, principal, page, size);
        return "fragments/water/page :: page";
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.TEXT_HTML_VALUE)
    public String delete(
            @PathVariable("id") Long id, Model model,
            @AuthenticationPrincipal OidcUser principal,
            @RequestParam(name = "page", required = false) Integer page,
            @RequestParam(name = "size", required = false) Integer size) {
        repository.deleteById(id);
        refreshDataInModel(model, principal, page, size);
        return "fragments/water/page :: page";
    }
}
