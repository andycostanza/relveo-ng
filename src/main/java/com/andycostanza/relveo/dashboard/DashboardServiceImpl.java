package com.andycostanza.relveo.dashboard;

import com.andycostanza.relveo.chart.ChartContainer;
import com.andycostanza.relveo.chart.service.ChartService;
import com.andycostanza.relveo.electricity.ElectricityIndexStatement;
import com.andycostanza.relveo.electricity.ElectricityIndexStatementRepository;
import com.andycostanza.relveo.heating.HeatingIndexStatement;
import com.andycostanza.relveo.heating.HeatingIndexStatementRepository;
import com.andycostanza.relveo.userpreference.UserPreferenceRepository;
import com.andycostanza.relveo.water.WaterIndexStatement;
import com.andycostanza.relveo.water.WaterIndexStatementRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class DashboardServiceImpl implements DashboardService {
    private final ElectricityIndexStatementRepository electricityIndexStatementRepository;
    private final HeatingIndexStatementRepository heatingIndexStatementRepository;
    private final WaterIndexStatementRepository waterIndexStatementRepository;
    private final UserPreferenceRepository userPreferenceRepository;
    private final ChartService chartService;


    public DashboardServiceImpl(ElectricityIndexStatementRepository electricityIndexStatementRepository, HeatingIndexStatementRepository heatingIndexStatementRepository, WaterIndexStatementRepository waterIndexStatementRepository, UserPreferenceRepository userPreferenceRepository, ChartService chartService) {
        this.electricityIndexStatementRepository = electricityIndexStatementRepository;
        this.heatingIndexStatementRepository = heatingIndexStatementRepository;
        this.waterIndexStatementRepository = waterIndexStatementRepository;
        this.userPreferenceRepository = userPreferenceRepository;
        this.chartService = chartService;
    }

    @Override
    public List<Integer> getYearsFilter(int currentYear) {
        List<Integer> listOfYear = new ArrayList<>();
        listOfYear.add(currentYear);
        Optional<ElectricityIndexStatement> lastElectricityStatementIndex = electricityIndexStatementRepository.findFirstByOrderByStatementDateAsc();
        lastElectricityStatementIndex.ifPresent(statement -> listOfYear.add(statement.getStatementDate()
                .getYear()));
        Optional<HeatingIndexStatement> lastHeatingStatementIndex = heatingIndexStatementRepository.findFirstByOrderByStatementDateAsc();
        lastHeatingStatementIndex.ifPresent(statement -> listOfYear.add(statement.getStatementDate()
                .getYear()));
        Optional<WaterIndexStatement> lastWaterStatementIndex = waterIndexStatementRepository.findFirstByOrderByStatementDateAsc();
        lastWaterStatementIndex.ifPresent(statement -> listOfYear.add(statement.getStatementDate()
                .getYear()));
        Collections.sort(listOfYear);
        if (listOfYear.get(0) == currentYear) {
            return Collections.singletonList(currentYear);
        } else {
            List<Integer> yearsFilter = new LinkedList<>();
            for (int i = currentYear; i >= listOfYear.get(0); i--) {
                yearsFilter.add(i);
            }
            return yearsFilter;
        }
    }

    @Override
    public List<ChartContainer> getConsumptionForDashboard(DashboardFilter filter, String userId) {
        return chartService.dashboardConsumptionCalculator(filter,
                electricityIndexStatementRepository.findByUserIdAndStatementDateBetweenOrderByStatementDateDesc(userId, LocalDate.of(
                        filter.year(),
                        1,
                        1), LocalDate.of(filter.year(), 12, 31)),
                heatingIndexStatementRepository.findByUserIdAndStatementDateBetweenOrderByStatementDateDesc(userId, LocalDate.of(filter.year(),
                        1,
                        1), LocalDate.of(filter.year(), 12, 31)),
                userPreferenceRepository.getReferenceById(userId),
                waterIndexStatementRepository.findByUserIdAndStatementDateBetweenOrderByStatementDateDesc(userId, LocalDate.of(filter.year(),
                        1,
                        1), LocalDate.of(filter.year(), 12, 31)));
    }


}
