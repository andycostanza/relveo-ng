package com.andycostanza.relveo.dashboard;

public record DashboardFilter(int year, DashboarChartType type, DashboarChartView view) {
}
