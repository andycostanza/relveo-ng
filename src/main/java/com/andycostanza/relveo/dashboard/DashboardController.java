package com.andycostanza.relveo.dashboard;

import com.andycostanza.relveo.chart.ChartContainer;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {
    private final DashboardService service;

    public DashboardController(DashboardService service) {
        this.service = service;
    }

    private static String getUserId(OidcUser principal) {
        return Base64.getEncoder()
                .encodeToString(principal.getSubject()
                        .getBytes(Charset.defaultCharset()));
    }

    @ModelAttribute("requestURI")
    public String requestURI(final HttpServletRequest request) {
        return request.getRequestURI();
    }

    private static List<DashboarChartView> getViewFilter() {
        return List.of(DashboarChartView.GLOBAL, DashboarChartView.ELECTRICITY, DashboarChartView.HEATING, DashboarChartView.WATER);
    }

    @GetMapping
    public String dashboard(Model model, @AuthenticationPrincipal OidcUser principal) {
        if (principal != null) {
            model.addAttribute("profile", principal.getClaims());
            DashboardFilter filter = new DashboardFilter(LocalDate.now()
                    .getYear(), DashboarChartType.ANNUAL, DashboarChartView.GLOBAL);
            List<ChartContainer> dataset = service.getConsumptionForDashboard(filter, getUserId(principal));
            model.addAttribute("filter", filter);
            model.addAttribute("dataset", dataset);
        }
        model.addAttribute("yearsFilter", getYearsFilter());
        model.addAttribute("viewFilter", getViewFilter());
        return "dashboard";
    }

    private List<Integer> getYearsFilter() {
        return service.getYearsFilter(LocalDate.now()
                .getYear());
    }

    @PostMapping
    public String dashboardFiltered(
            @ModelAttribute DashboardFilter filter, Model model, @AuthenticationPrincipal OidcUser principal) {
        if (principal != null) {
            model.addAttribute("profile", principal.getClaims());
            List<ChartContainer> dataset = service.getConsumptionForDashboard(filter, getUserId(principal));
            model.addAttribute("dataset", dataset);
        }
        model.addAttribute("yearsFilter", getYearsFilter());
        model.addAttribute("viewFilter", getViewFilter());
        model.addAttribute("filter", filter);

        return "dashboard";
    }
}
