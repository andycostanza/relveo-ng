package com.andycostanza.relveo.dashboard;

import com.andycostanza.relveo.chart.ChartContainer;

import java.io.Serializable;
import java.util.List;

public interface DashboardService extends Serializable {
    List<Integer> getYearsFilter(int currentYear);

    List<ChartContainer> getConsumptionForDashboard(DashboardFilter filter, String userId);

}
