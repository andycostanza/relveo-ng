package com.andycostanza.relveo.userpreference;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

@Entity
@Table(name = "user_preferences", schema = "relveo")
public class UserPreference implements Serializable {
    @Id
    private String id;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate lastLogin;
    private LocalTime lastLoginTime;
    @Enumerated(EnumType.STRING)
    private TankType tankType;
    private BigDecimal widthOfTank;
    private BigDecimal lengthOfTank;
    private BigDecimal diameterOfTank;

    public UserPreference() {
    }

    public UserPreference(String id, LocalDate lastLogin, LocalTime lastLoginTime, TankType tankType, BigDecimal widthOfTank, BigDecimal lengthOfTank, BigDecimal diameterOfTank) {
        this.id = id;
        this.lastLogin = lastLogin;
        this.lastLoginTime = lastLoginTime;
        this.tankType = tankType;
        this.widthOfTank = widthOfTank;
        this.lengthOfTank = lengthOfTank;
        this.diameterOfTank = diameterOfTank;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Transient
    public Optional<BigDecimal> getSurfaceOfTank() {
        Assert.notNull(this.widthOfTank, "The width of tank should not be null to calculate the surface");
        Assert.notNull(this.lengthOfTank, "The length of tank should not be null to calculate the surface");
        Assert.notNull(this.tankType, "The type of tank should not be null to calculate the surface");
        if (TankType.CUBOID.equals(this.tankType)) {
            return Optional.of(this.widthOfTank.multiply(this.lengthOfTank));
        } else {
            //not yet implemented
            return Optional.empty();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDate lastLogin) {
        this.lastLogin = lastLogin;
    }

    public LocalTime getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(LocalTime lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public TankType getTankType() {
        return tankType;
    }

    public void setTankType(TankType tankType) {
        this.tankType = tankType;
    }

    public BigDecimal getWidthOfTank() {
        return widthOfTank;
    }

    public void setWidthOfTank(BigDecimal widthOfTank) {
        this.widthOfTank = widthOfTank;
    }

    public BigDecimal getLengthOfTank() {
        return lengthOfTank;
    }

    public void setLengthOfTank(BigDecimal lengthOfTank) {
        this.lengthOfTank = lengthOfTank;
    }

    public BigDecimal getDiameterOfTank() {
        return diameterOfTank;
    }

    public void setDiameterOfTank(BigDecimal diameterOfTank) {
        this.diameterOfTank = diameterOfTank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserPreference that)) return false;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getLastLogin() != null ? !getLastLogin().equals(that.getLastLogin()) : that.getLastLogin() != null)
            return false;
        if (getLastLoginTime() != null ? !getLastLoginTime().equals(that.getLastLoginTime()) : that.getLastLoginTime() != null)
            return false;
        if (getTankType() != that.getTankType()) return false;
        if (getWidthOfTank() != null ? !getWidthOfTank().equals(that.getWidthOfTank()) : that.getWidthOfTank() != null)
            return false;
        if (getLengthOfTank() != null ? !getLengthOfTank().equals(that.getLengthOfTank()) : that.getLengthOfTank() != null)
            return false;
        return getDiameterOfTank() != null ? getDiameterOfTank().equals(that.getDiameterOfTank()) : that.getDiameterOfTank() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getLastLogin() != null ? getLastLogin().hashCode() : 0);
        result = 31 * result + (getLastLoginTime() != null ? getLastLoginTime().hashCode() : 0);
        result = 31 * result + (getTankType() != null ? getTankType().hashCode() : 0);
        result = 31 * result + (getWidthOfTank() != null ? getWidthOfTank().hashCode() : 0);
        result = 31 * result + (getLengthOfTank() != null ? getLengthOfTank().hashCode() : 0);
        result = 31 * result + (getDiameterOfTank() != null ? getDiameterOfTank().hashCode() : 0);
        return result;
    }

    public static class Builder {
        private String id;
        private LocalDate lastLogin;
        private LocalTime lastLoginTime;
        private TankType tankType;
        private BigDecimal widthOfTank;
        private BigDecimal lengthOfTank;
        private BigDecimal diameterOfTank;

        public Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder lastLogin(LocalDate lastLogin) {
            this.lastLogin = lastLogin;
            return this;
        }

        public Builder lastLoginTime(LocalTime lastLoginTime) {
            this.lastLoginTime = lastLoginTime;
            return this;
        }

        public Builder tankType(TankType tankType) {
            this.tankType = tankType;
            return this;
        }

        public Builder widthOfTank(BigDecimal widthOfTank) {
            this.widthOfTank = widthOfTank;
            return this;
        }

        public Builder lengthOfTank(BigDecimal lengthOfTank) {
            this.lengthOfTank = lengthOfTank;
            return this;
        }

        public Builder diameterOfTank(BigDecimal diameterOfTank) {
            this.diameterOfTank = diameterOfTank;
            return this;
        }

        public UserPreference build() {
            return new UserPreference(id,
                    lastLogin,
                    lastLoginTime,
                    tankType,
                    widthOfTank,
                    lengthOfTank,
                    diameterOfTank);
        }
    }
}
