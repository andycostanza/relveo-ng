package com.andycostanza.relveo.userpreference;

public enum TankType {
    CUBOID, VERTICAL_CYLINDER, HORIZONTAL_CYLINDER  // @See https://www.mathsisfun.com/geometry/cylinder-horizontal-volume.html
}
