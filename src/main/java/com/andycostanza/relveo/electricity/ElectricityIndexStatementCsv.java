package com.andycostanza.relveo.electricity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

public record ElectricityIndexStatementCsv(@JsonFormat(pattern = "yyyy-MM-dd") LocalDate statementDate,
                                           BigDecimal dayIndex, BigDecimal nightIndex) {
}
