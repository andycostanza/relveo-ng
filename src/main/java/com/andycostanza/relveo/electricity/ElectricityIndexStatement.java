package com.andycostanza.relveo.electricity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by andy on 24/03/17.
 */
@Entity
@Table(name = "electricity_index_statements", schema = "relveo")
public class ElectricityIndexStatement implements Serializable, Comparable<ElectricityIndexStatement> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate statementDate;
    private BigDecimal dayIndex;
    private BigDecimal nightIndex;
    private String userId;

    public ElectricityIndexStatement() {
    }

    public ElectricityIndexStatement(Long id, LocalDate statementDate, BigDecimal dayIndex, BigDecimal nightIndex, String userId) {
        this.id = id;
        this.statementDate = statementDate;
        this.dayIndex = dayIndex;
        this.nightIndex = nightIndex;
        this.userId = userId;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public int compareTo(ElectricityIndexStatement e) {
        return getStatementDate().compareTo(e.getStatementDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStatementDate() {
        return statementDate;
    }

    public void setStatementDate(LocalDate statementDate) {
        this.statementDate = statementDate;
    }

    public BigDecimal getDayIndex() {
        return dayIndex;
    }

    public void setDayIndex(BigDecimal dayIndex) {
        this.dayIndex = dayIndex;
    }

    public BigDecimal getNightIndex() {
        return nightIndex;
    }

    public void setNightIndex(BigDecimal nightIndex) {
        this.nightIndex = nightIndex;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ElectricityIndexStatement that)) return false;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getStatementDate() != null ? !getStatementDate().equals(that.getStatementDate()) : that.getStatementDate() != null)
            return false;
        if (getDayIndex() != null ? !getDayIndex().equals(that.getDayIndex()) : that.getDayIndex() != null)
            return false;
        if (getNightIndex() != null ? !getNightIndex().equals(that.getNightIndex()) : that.getNightIndex() != null)
            return false;
        return getUserId() != null ? getUserId().equals(that.getUserId()) : that.getUserId() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getStatementDate() != null ? getStatementDate().hashCode() : 0);
        result = 31 * result + (getDayIndex() != null ? getDayIndex().hashCode() : 0);
        result = 31 * result + (getNightIndex() != null ? getNightIndex().hashCode() : 0);
        result = 31 * result + (getUserId() != null ? getUserId().hashCode() : 0);
        return result;
    }

    public static class Builder {
        private Long id;
        private LocalDate statementDate;
        private BigDecimal dayIndex;
        private BigDecimal nightIndex;
        private String userId;

        public Builder() {
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder statementDate(LocalDate statementDate) {
            this.statementDate = statementDate;
            return this;
        }

        public Builder dayIndex(BigDecimal dayIndex) {
            this.dayIndex = dayIndex;
            return this;
        }

        public Builder nightIndex(BigDecimal nightIndex) {
            this.nightIndex = nightIndex;
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public ElectricityIndexStatement build() {
            return new ElectricityIndexStatement(id, statementDate, dayIndex, nightIndex, userId);
        }
    }
}


