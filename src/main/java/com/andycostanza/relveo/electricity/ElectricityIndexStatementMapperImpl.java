package com.andycostanza.relveo.electricity;


import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ElectricityIndexStatementMapperImpl implements ElectricityIndexStatementMapper {
    private final ElectricityIndexStatementRepository repository;

    public ElectricityIndexStatementMapperImpl(ElectricityIndexStatementRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<ElectricityIndexStatement> toEntities(List<ElectricityIndexStatementCsv> csv, String userId) {
        return csv.stream()
                .map(electricityIndexStatementCsv -> {
                    Optional<ElectricityIndexStatement> entity = repository.findByUserIdAndStatementDate(userId,
                            electricityIndexStatementCsv.statementDate());
                    if (entity.isPresent()) {
                        entity.get()
                                .setDayIndex(electricityIndexStatementCsv.dayIndex());
                        entity.get()
                                .setNightIndex(electricityIndexStatementCsv.nightIndex());
                        return entity.get();
                    } else {
                        return ElectricityIndexStatement.builder()
                                .statementDate(electricityIndexStatementCsv.statementDate())
                                .dayIndex(electricityIndexStatementCsv.dayIndex())
                                .nightIndex(electricityIndexStatementCsv.nightIndex())
                                .userId(userId)
                                .build();
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<ElectricityIndexStatementCsv> toCsv(List<ElectricityIndexStatement> entities) {
        return entities.stream()
                .map(electricityIndexStatement -> new ElectricityIndexStatementCsv(electricityIndexStatement.getStatementDate(),
                        electricityIndexStatement.getDayIndex(),
                        electricityIndexStatement.getNightIndex()))
                .collect(Collectors.toList());
    }
}
