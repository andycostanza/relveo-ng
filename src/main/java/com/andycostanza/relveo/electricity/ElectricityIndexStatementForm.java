package com.andycostanza.relveo.electricity;

import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.time.LocalDate;

public record ElectricityIndexStatementForm(Long id,
                                            @NotNull
                                            LocalDate statementDate,
                                            @NotNull
                                            BigDecimal dayIndex,
                                            @NotNull
                                            BigDecimal nightIndex,
                                            String userId) {
    public ElectricityIndexStatement toEntity() {
        return new ElectricityIndexStatement(id, statementDate, dayIndex, nightIndex, userId);
    }
}
