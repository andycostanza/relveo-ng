package com.andycostanza.relveo.electricity;

import java.io.Serializable;
import java.util.List;

public interface ElectricityIndexStatementMapper extends Serializable {
    List<ElectricityIndexStatement> toEntities(List<ElectricityIndexStatementCsv> csv, String userId);

    List<ElectricityIndexStatementCsv> toCsv(List<ElectricityIndexStatement> entities);
}
