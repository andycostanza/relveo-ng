package com.andycostanza.relveo.controller;

import com.andycostanza.relveo.userpreference.TankType;
import com.andycostanza.relveo.userpreference.UserPreference;
import com.andycostanza.relveo.userpreference.UserPreferenceRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Base64;
import java.util.Optional;

/**
 * Controller for requests to the {@code /profile} resource. Populates the model with the claims from the
 * {@linkplain OidcUser} for use by the view.
 */
@Controller
@RequestMapping("/settings")
public class ProfileController {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final UserPreferenceRepository repository;

    public ProfileController(UserPreferenceRepository repository) {
        this.repository = repository;
    }

    private static String getUserId(OidcUser principal) {
        return Base64.getEncoder()
                .encodeToString(principal.getSubject()
                        .getBytes(Charset.defaultCharset()));
    }

    @ModelAttribute("requestURI")
    public String requestURI(final HttpServletRequest request) {
        return request.getRequestURI();
    }

    @GetMapping
    public String settings(Model model, @AuthenticationPrincipal OidcUser principal) {
        String userId = getUserId(principal);
        Optional<UserPreference> userPreference = repository.findById(userId);

        model.addAttribute("userPreference",
                userPreference.orElse(UserPreference.builder()
                        .id(userId)
                        .tankType(TankType.CUBOID)
                        .lengthOfTank(BigDecimal.valueOf(225D))
                        .widthOfTank(BigDecimal.valueOf(135D))
                        .build()));

        model.addAttribute("profile", principal.getClaims());
        return "settings";
    }

    @PostMapping
    public String upsert(@ModelAttribute UserPreference userPreference, @AuthenticationPrincipal OidcUser principal) {
        String userId = getUserId(principal);
        userPreference.setId(userId);
        userPreference.setLastLogin(LocalDate.now());
        userPreference.setLastLoginTime(LocalTime.now());
        repository.save(userPreference);
        return "redirect:/settings";
    }

}
