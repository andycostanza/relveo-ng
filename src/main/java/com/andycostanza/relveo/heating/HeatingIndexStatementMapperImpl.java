package com.andycostanza.relveo.heating;


import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service

public class HeatingIndexStatementMapperImpl implements HeatingIndexStatementMapper {
    private final HeatingIndexStatementRepository repository;

    public HeatingIndexStatementMapperImpl(HeatingIndexStatementRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<HeatingIndexStatement> toEntities(List<HeatingIndexStatementCsv> csv, String userId) {
        return csv.stream()
                .map(heatingIndexStatementCsv -> {
                    Optional<HeatingIndexStatement> entity = repository.findByUserIdAndStatementDate(userId,
                            heatingIndexStatementCsv.statementDate());
                    if (entity.isPresent()) {
                        entity.get()
                                .setHeightOfTank(heatingIndexStatementCsv.heightOfTank());
                        return entity.get();
                    } else {
                        return HeatingIndexStatement.builder()
                                .statementDate(heatingIndexStatementCsv.statementDate())
                                .heightOfTank(heatingIndexStatementCsv.heightOfTank())
                                .userId(userId)
                                .build();
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<HeatingIndexStatementCsv> toCsv(List<HeatingIndexStatement> entities) {
        return entities.stream()
                .map(heatingIndexStatement -> new HeatingIndexStatementCsv(heatingIndexStatement.getStatementDate(),
                        heatingIndexStatement.getHeightOfTank()))
                .collect(Collectors.toList());
    }
}
