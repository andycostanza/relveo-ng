package com.andycostanza.relveo.heating;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

public record HeatingIndexStatementCsv(@JsonFormat(pattern = "yyyy-MM-dd") LocalDate statementDate,
                                       BigDecimal heightOfTank) {
}
