package com.andycostanza.relveo.heating;

import java.io.Serializable;
import java.util.List;

public interface HeatingIndexStatementMapper extends Serializable {
    List<HeatingIndexStatement> toEntities(List<HeatingIndexStatementCsv> csv, String userId);

    List<HeatingIndexStatementCsv> toCsv(List<HeatingIndexStatement> entities);
}
