package com.andycostanza.relveo.heating;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by andy on 24/03/17.
 */
@Repository
public interface HeatingIndexStatementRepository extends JpaRepository<HeatingIndexStatement, Long> {
    List<HeatingIndexStatement> findTop53ByUserIdOrderByStatementDateDesc(String userId);

    Page<HeatingIndexStatement> findByUserId(String userId, Pageable page);

    List<HeatingIndexStatement> findByUserIdOrderByStatementDateDesc(String userId);

    Optional<HeatingIndexStatement> findByUserIdAndStatementDate(String userId, LocalDate statementDate);

    Optional<HeatingIndexStatement> findFirstByOrderByStatementDateAsc();

    List<HeatingIndexStatement> findByUserIdAndStatementDateBetweenOrderByStatementDateDesc(String userId, LocalDate from, LocalDate to);
}
