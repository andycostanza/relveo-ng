package com.andycostanza.relveo.heating;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by andy on 24/03/17.
 */
@Entity
@Table(name = "heating_index_statements", schema = "relveo")
public class HeatingIndexStatement implements Serializable, Comparable<HeatingIndexStatement> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate statementDate;
    private BigDecimal heightOfTank;
    private String userId;

    public HeatingIndexStatement() {
    }

    public HeatingIndexStatement(Long id, LocalDate statementDate, BigDecimal heightOfTank, String userId) {
        this.id = id;
        this.statementDate = statementDate;
        this.heightOfTank = heightOfTank;
        this.userId = userId;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public int compareTo(HeatingIndexStatement h) {
        if (getStatementDate() == null) {
            return -1;
        }
        return getStatementDate().compareTo(h.getStatementDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStatementDate() {
        return statementDate;
    }

    public void setStatementDate(LocalDate statementDate) {
        this.statementDate = statementDate;
    }

    public BigDecimal getHeightOfTank() {
        return heightOfTank;
    }

    public void setHeightOfTank(BigDecimal heightOfTank) {
        this.heightOfTank = heightOfTank;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HeatingIndexStatement that)) return false;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getStatementDate() != null ? !getStatementDate().equals(that.getStatementDate()) : that.getStatementDate() != null)
            return false;
        if (getHeightOfTank() != null ? !getHeightOfTank().equals(that.getHeightOfTank()) : that.getHeightOfTank() != null)
            return false;
        return getUserId() != null ? getUserId().equals(that.getUserId()) : that.getUserId() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getStatementDate() != null ? getStatementDate().hashCode() : 0);
        result = 31 * result + (getHeightOfTank() != null ? getHeightOfTank().hashCode() : 0);
        result = 31 * result + (getUserId() != null ? getUserId().hashCode() : 0);
        return result;
    }

    public static class Builder {
        private Long id;
        private LocalDate statementDate;
        private BigDecimal heightOfTank;
        private String userId;

        public Builder() {
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder statementDate(LocalDate statementDate) {
            this.statementDate = statementDate;
            return this;
        }

        public Builder heightOfTank(BigDecimal heightOfTank) {
            this.heightOfTank = heightOfTank;
            return this;
        }

        public Builder userId(String userId) {
            this.userId = userId;
            return this;
        }

        public HeatingIndexStatement build() {
            return new HeatingIndexStatement(id, statementDate, heightOfTank, userId);
        }
    }
}
