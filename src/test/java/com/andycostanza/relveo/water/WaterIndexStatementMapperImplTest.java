package com.andycostanza.relveo.water;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
class WaterIndexStatementMapperImplTest {

    WaterIndexStatementMapper mapper;
    @Mock
    WaterIndexStatementRepository repository;

    @BeforeEach
    void setUp() {
        mapper = new WaterIndexStatementMapperImpl(repository);
    }

    @Test
    @DisplayName("should return new WaterIndexStatement if it doesn't find in the repository")
    void toEntitiesTest10() {
        //given
        Mockito.when(repository.findByUserIdAndStatementDate(Mockito.anyString(), Mockito.any(LocalDate.class)))
                .thenReturn(Optional.empty());
        //when
        LocalDate now = LocalDate.now();
        WaterIndexStatementCsv csv = new WaterIndexStatementCsv(now, BigDecimal.ONE);
        List<WaterIndexStatement> result = mapper.toEntities(Collections.singletonList(csv), "userId");
        //then
        assertAll("result",
                () -> assertNotNull(result),
                () -> assertEquals(1, result.size()),
                () -> assertNull(result.get(0)
                        .getId()),
                () -> assertEquals(now,
                        result.get(0)
                                .getStatementDate()),
                () -> assertEquals(BigDecimal.ONE,
                        result.get(0)
                                .getWaterIndex()),
                () -> assertEquals("userId",
                        result.get(0)
                                .getUserId()));
    }

    @Test
    @DisplayName("should return an updated existing WaterIndexStatement if it find in the repository")
    void toEntitiesTest20() {
        //given
        LocalDate now = LocalDate.now();
        WaterIndexStatement waterIndexStatement = WaterIndexStatement.builder()
                .id(1L)
                .userId("userId")
                .waterIndex(BigDecimal.ZERO)
                .statementDate(now)
                .build();
        Mockito.when(repository.findByUserIdAndStatementDate(Mockito.anyString(), Mockito.any(LocalDate.class)))
                .thenReturn(Optional.of(waterIndexStatement));
        //when

        WaterIndexStatementCsv csv = new WaterIndexStatementCsv(now, BigDecimal.ONE);
        List<WaterIndexStatement> result = mapper.toEntities(Collections.singletonList(csv), "userId");
        //then
        assertAll("result",
                () -> assertNotNull(result),
                () -> assertEquals(1, result.size()),
                () -> assertEquals(1L,
                        result.get(0)
                                .getId()),
                () -> assertEquals(now,
                        result.get(0)
                                .getStatementDate()),
                () -> assertEquals(BigDecimal.ONE,
                        result.get(0)
                                .getWaterIndex()),
                () -> assertEquals("userId",
                        result.get(0)
                                .getUserId()));
    }

    @Test
    @DisplayName("should return a List of WaterIndexStatementCsv")
    void toCsvTest10() {
        //given
        LocalDate now = LocalDate.now();
        WaterIndexStatement waterIndexStatement = WaterIndexStatement.builder()
                .id(1L)
                .userId("userId")
                .waterIndex(BigDecimal.ONE)
                .statementDate(now)
                .build();
        //when
        List<WaterIndexStatementCsv> result = mapper.toCsv(Collections.singletonList(waterIndexStatement));
        //then
        assertAll("result",
                () -> assertNotNull(result),
                () -> assertEquals(1, result.size()),
                () -> assertEquals(now,
                        result.get(0)
                                .statementDate()),
                () -> assertEquals(BigDecimal.ONE,
                        result.get(0)
                                .waterIndex()));
    }
}