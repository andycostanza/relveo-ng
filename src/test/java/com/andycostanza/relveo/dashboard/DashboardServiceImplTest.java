package com.andycostanza.relveo.dashboard;

import com.andycostanza.relveo.chart.service.ChartService;
import com.andycostanza.relveo.electricity.ElectricityIndexStatement;
import com.andycostanza.relveo.electricity.ElectricityIndexStatementRepository;
import com.andycostanza.relveo.heating.HeatingIndexStatement;
import com.andycostanza.relveo.heating.HeatingIndexStatementRepository;
import com.andycostanza.relveo.userpreference.UserPreferenceRepository;
import com.andycostanza.relveo.water.WaterIndexStatement;
import com.andycostanza.relveo.water.WaterIndexStatementRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class DashboardServiceImplTest {
    DashboardServiceImpl service;
    @Mock
    ElectricityIndexStatementRepository electricityIndexStatementRepository;
    @Mock
    HeatingIndexStatementRepository heatingIndexStatementRepository;
    @Mock
    WaterIndexStatementRepository waterIndexStatementRepository;
    @Mock
    UserPreferenceRepository userPreferenceRepository;
    @Mock
    ChartService chartService;

    @BeforeEach
    void setUp() {
        service = new DashboardServiceImpl(electricityIndexStatementRepository,
                heatingIndexStatementRepository,
                waterIndexStatementRepository,
                userPreferenceRepository,
                chartService);
    }

    @Test
    @DisplayName("should return singleton list with current year if there's no data")
    void getYearsFilter10() {
        Mockito.doReturn(Optional.empty())
                .when(electricityIndexStatementRepository)
                .findFirstByOrderByStatementDateAsc();
        Mockito.doReturn(Optional.empty())
                .when(heatingIndexStatementRepository)
                .findFirstByOrderByStatementDateAsc();
        Mockito.doReturn(Optional.empty())
                .when(waterIndexStatementRepository)
                .findFirstByOrderByStatementDateAsc();
        int currentYear = LocalDate.now()
                .getYear();
        List<Integer> result = service.getYearsFilter(currentYear);
        Assertions.assertAll("result",
                () -> Assertions.assertEquals(result.size(), 1),
                () -> Assertions.assertEquals(result.get(0), currentYear));
    }

    @Test
    @DisplayName("should return list of year until current year")
    void getYearsFilter20() {
        Mockito.doReturn(Optional.of(ElectricityIndexStatement.builder()
                        .statementDate(LocalDate.of(2020, 1, 1))
                        .build()))
                .when(electricityIndexStatementRepository)
                .findFirstByOrderByStatementDateAsc();
        Mockito.doReturn(Optional.of(HeatingIndexStatement.builder()
                        .statementDate(LocalDate.of(2020, 1, 1))
                        .build()))
                .when(heatingIndexStatementRepository)
                .findFirstByOrderByStatementDateAsc();
        Mockito.doReturn(Optional.of(WaterIndexStatement.builder()
                        .statementDate(LocalDate.of(2020, 1, 1))
                        .build()))
                .when(waterIndexStatementRepository)
                .findFirstByOrderByStatementDateAsc();
        int currentYear = 2023;
        List<Integer> result = service.getYearsFilter(currentYear);
        Assertions.assertAll("result",
                () -> Assertions.assertEquals(result.size(), 4),
                () -> Assertions.assertEquals(result, List.of(2023, 2022, 2021, 2020)));
    }

    @Test
    @DisplayName("should return list of year until current year with different statement date")
    void getYearsFilter30() {
        Mockito.doReturn(Optional.of(ElectricityIndexStatement.builder()
                        .statementDate(LocalDate.of(2020, 1, 1))
                        .build()))
                .when(electricityIndexStatementRepository)
                .findFirstByOrderByStatementDateAsc();
        Mockito.doReturn(Optional.of(HeatingIndexStatement.builder()
                        .statementDate(LocalDate.of(2019, 1, 1))
                        .build()))
                .when(heatingIndexStatementRepository)
                .findFirstByOrderByStatementDateAsc();
        Mockito.doReturn(Optional.of(WaterIndexStatement.builder()
                        .statementDate(LocalDate.of(2018, 1, 1))
                        .build()))
                .when(waterIndexStatementRepository)
                .findFirstByOrderByStatementDateAsc();
        int currentYear = 2023;
        List<Integer> result = service.getYearsFilter(currentYear);
        Assertions.assertAll("result",
                () -> Assertions.assertEquals(result.size(), 6),
                () -> Assertions.assertEquals(result, List.of(2023, 2022, 2021, 2020, 2019, 2018)));
    }

}